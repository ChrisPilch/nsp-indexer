<?php

/*
 * For stand alone use copy to config.php and modify from there.  for docker use override with environmental variables
 */

ini_set('memory_limit', '512M'); /* To prevent low memory errors - may not override php.ini limit is set there. if so edit php.ini with memory limit >= 512M */

$gameDir = "/data/games"; /* Absolute Files Path, no trailing slash */
$contentUrl = "/games"; /* Files URL, no trailing slash */
$allowedExtensions = explode(",", 'nsp,xci,nsz,xcz'); /* extensions for files that nsp-indexer is allowed to process */
$skipExtension = "txt"; /* file extension used to mute alert for an update or dlc */
$enableNetInstall = true; /* Enable Net Install feature */
$enableRename = true; /* Enable Rename feature */
$switchIp = "192.168.1.50"; /* Switch IP address for Net Install */
$netInstallSrc = false; /* Set to e.g. '192.168.0.1:80' to override source address for Net Install */
$showWarnings = true; /* Show configuration warnings on page load */
$keyFile = "/config/prod.keys"; /* Path to 'prod.keys', must be readable by the webserver/php, but KEEP IT SECURE via .htaccess or similar */
$titleDbFolder = "/config/nut/titledb"; /* Path to titledb folder in nut installation */
$extPort = "8000"; /* port that is forwarded to nginx */
