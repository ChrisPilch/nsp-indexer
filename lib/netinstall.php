<?php

/**
 * Net Installer for Switch NSP XCI NSZ and XCZ files
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload

require_once dirname(__FILE__) . '/../config.default.php';
if (file_exists(dirname(__FILE__) . '/../config.php')) {
    include_once dirname(__FILE__) . '/../config.php';
}

global $netInstallSrc;
if (!is_string($netInstallSrc) && $netInstallSrc) {
    $netInstallSrc = $_SERVER['SERVER_ADDR'] . ':' . $_SERVER['SERVER_PORT'];
}

$dstAddr = $_POST["dstAddr"];
$dstPort = 2000;

$strPayload = "";
foreach ($_POST["listFiles"] as $key => $file) {
    $strPayload .= $netInstallSrc . $contentUrl . '/' . $file . "\n";
}

$strPayload = mb_convert_encoding($strPayload, 'ASCII');
$payload = pack("N", strlen($strPayload)) . $strPayload;

$status = new stdClass();
$status->int = -1;

$socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    $status->msg = "Error Creating Socket";
} elseif (@socket_connect($socket, $dstAddr, $dstPort) === false) {
    $status->msg = "Error Connecting to Socket";
} elseif (@socket_write($socket, $payload, strlen($payload)) === false) {
    $status->msg = "Error Writing to Socket";
} else {
    $status->msg = "OK";
    $status->int = 0;
}

header("Content-Type: application/json");
echo json_encode($status);
