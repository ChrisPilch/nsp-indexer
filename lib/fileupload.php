<?php

/**
 * File Uploader for Switch NSP XCI NSZ and XCZ files
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload

require_once dirname(__FILE__) . '/../config.default.php';
if (file_exists(dirname(__FILE__) . '/../config.php')) {
    include_once dirname(__FILE__) . '/../config.php';
}

\Flow\Autoloader::register();

if (1 == mt_rand(1, 100)) {
    \Flow\Uploader::pruneChunks(__DIR__ . '/../cache/tmp_upload');
}

$config = new \Flow\Config(
    array(
    'tempDir' => __DIR__ . '/../cache/tmp_upload'
    )
);
$request = new \Flow\Request();
if (\Flow\Basic::save($gameDir . DIRECTORY_SEPARATOR . $request->getFileName(), $config, $request)) {}
