<?php

/**
 * CTRCOUNTERGMP class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to export the ctr as a padded binary string
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

class CTRCOUNTERGMP
{
    public $ctr;

    /**
     * Creates properties and values for object
     *
     * @param string $ctr ctr to process
     *
     * @return mixed padded binary ctr
     */
    public function __construct($ctr)
    {
        $this->ctr = gmp_import($ctr);
    }

    /**
     * Function to add the the num and the ctr
     *
     * @param string $num num to add
     *
     * @return mixed sum of ctr and num
     */
    public function add($num)
    {
        $this->ctr = gmp_add($this->ctr, $num);
    }

    /**
     * Function to add the the num and the ctr
     *
     * @return padded binary string of the ctr
     */
    public function getCtr()
    {
        return str_pad(gmp_export($this->ctr), 16, chr(0), STR_PAD_LEFT);
    }
}
