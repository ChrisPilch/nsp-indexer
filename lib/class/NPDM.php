<?php

/**
 * ACID class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to get ACID header info
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class NPDM
{
    public $acid;
    public $magic;
    public $AcidSignatureKeyGeneration;
    public $flags;
    public $MainThreadPriority;
    public $MainThreadCoreNumber;
    public $version;
    public $AciOffset;
    public $AciSize;
    public $AcidOffset;
    public $AcidSize;

    /**
     * Creates properties and values for object
     *
     * @param string $data data to parse
     * @param string $size size of the ACID header
     *
     * @return mixed properties and values of ACID header info
     */
    public function __construct($data, $size)
    {
        $this->magic = substr($data, 0, 0x04);
        $this->AcidSignatureKeyGeneration = unpack("V", substr($data, 0x04, 0x04))[1];
        $this->flags = substr($data, 0x0c, 0x01);
        $this->MainThreadPriority = substr($data, 0x0e, 0x01);
        $this->MainThreadCoreNumber = substr($data, 0x0f, 0x01);
        $this->version = unpack("V", substr($data, 0x18, 0x04))[1];
        $this->AciOffset = unpack("V", substr($data, 0x70, 0x04))[1];
        $this->AciSize = unpack("V", substr($data, 0x74, 0x04))[1];
        $this->AcidOffset = unpack("V", substr($data, 0x78, 0x04))[1];
        $this->AcidSize = unpack("V", substr($data, 0x7c, 0x04))[1];
        $this->acid = new ACID(substr($data, $this->AcidOffset, $this->AcidSize), $this->AcidSize);
    }
}
