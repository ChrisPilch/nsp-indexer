<?php

/**
 * HFS0 class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to get language info
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class NACP
{
    public $langs;
    public $version;

    private const LANGSTRINGS = ["AmericanEnglish","BritishEnglish","Japanese","French","German","LatinAmericanSpanish","Spanish","Italian","Dutch","CanadianFrench","Portuguese","Russian","Korean","TraditionalChinese","SimplifiedChinese" ];

    /**
     * Creates array with langs in the file
     *
     * @param string $ncapcontents data used to determine languages
     *
     * @return array list of languages in the file
     */
    public function __construct($ncapcontents)
    {
        $this->langs = array();
        for ($i = 0; $i < 15; $i++) {
            $langtmp = new \stdClass();
            $langtmp->title = trim(substr($ncapcontents, 0 + (0x300 * $i), 0x200));
            $langtmp->publisher = trim(substr($ncapcontents, 0x200 + (0x300 * $i), 0x100));
            $langtmp->name = self::LANGSTRINGS[$i];
            $langtmp->present = false;
            $this->langs[] = $langtmp;
        }
        $this->getLanguages(unpack("V", substr($ncapcontents, 0x302C, 4))[1]);
        $this->version = trim(substr($ncapcontents, 0x3060, 0x10));
    }

    /**
     * Gets index of language based on the icon file name
     *
     * @param string $iconfile file to use as to determine index
     *
     * @return int index of the language in the array
     */
    public function getLangIdx($iconfile)
    {
        $iconlang = str_replace(".dat", "", str_replace("/icon_", "", $iconfile));
        return array_search($iconlang, self::LANGSTRINGS);
    }

    /**
     * Adds presence info about the languages into the array of languages
     *
     * @param array $supportedLanguages array of languages in the file
     *
     * @return array presence status of language against the master language list
     */
    private function getLanguages($supportedLanguages)
    {
        for ($i = 0; $i < 15; $i++) {
            if ($supportedLanguages & (1 << $i)) {
                $this->langs[$i]->present = true;
            }
        }
    }
}
