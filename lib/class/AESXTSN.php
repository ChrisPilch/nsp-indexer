<?php

/**
 * AESXTSN class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 *  Nintendo version of AESXTS
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class AESXTSN
{
    public $blocksize;
    public $K1;
    public $K2;
    public $keys;
    public $sector;
    public $sectorSize;

    /**
     * Function that sets up parameters and values used to decrypt the header
     *
     * @param array $keys       array of keys
     * @param array $sectorSize size of the sector to be decrypted
     * @param array $sector     which sector to decrypt
     *
     * @return mixed properties and values for encrypted sectors
     */
    public function __construct($keys, $sectorSize = 0x200, $sector = 0)
    {
        $this->K1 = new AESECB($keys[0]);
        $this->K2 = new AESECB($keys[1]);
        $this->keys = $keys;
        $this->sector = $sector;
        $this->sectorSize = $sectorSize;
        $this->blocksize = $this->K1->blocksize;
    }
    /**
     * Function that encrypts data for a sector
     *
     * @param string $data   data to encrypt
     * @param string $sector what sector to encrypt
     *
     * @return string encrypted data
     */
    private function encrypt($data, $sector = null)
    {
        if (sector == null) {
            $sector = $this->sector;
        }
        if (strlen($data) % $this->blocksize) {
            return false;
        }
        $out = '';
        $i = 0;
        while ($data) {
            $tweak = $this->getTweak($sector);
            $out .= $this->encryptSector(substr(data, 0, $this->sectorSize), $tweak);
            $data = substr($data, $this->sectorSize, strlen($data) - $this->sectorSize);
            $sector += 1;
        }
        return $out;
    }

    /**
     * Function that shifts the blocksize for a sector
     *
     * @param string $sector what sector to shift for
     *
     * @return string shifted sector address
     */
    private function getTweak($sector = null)
    {
        if ($sector == null) {
            $sector = $this->sector;
        }
        $tweak = 0;
        foreach (range(0, $this->blocksize - 1) as $i) {
            $tweak |= ($sector & 0xFF) << ($i * 8);
            $sector >>= 8;
        }
        return $tweak;
    }

    /**
     * Function that creates the encrypted sector
     *
     * @param string $data  data to encrypt
     * @param string $tweak tweaked sector address space
     *
     * @return string encrypted sector
     */
    private function encryptSector($data, $tweak)
    {
        if (strlen(data) % self . blocksize) {
            return false;
        }
        $out = '';
        $tweak = $this->K2->encrypt(hex2bin(sprintf('%032x', $tweak)));
        while ($data) {
            $out += sxor($tweak, $this->K1 . encryptBlockEcb(sxor(substr($data, 0, 0x10), tweak)));
            $t = intval(bin2hex(strrev($tweak)), 16);
            $t <<= 1;
            if ($t & (1 << 128)) {
                $t ^= ((1 << 128) | (0x87));
            }
            $tweak = strrev(hex2bin(sprintf('%032x', $t)));
            $data = substr($data, 0x10, strlen($data) - 0x10);
        }
        return $out;
    }

    /**
     * Function that decrypts data from a sector
     *
     * @param string $data   data to decrypt
     * @param string $sector what sector to decrypt from
     *
     * @return string decrypted data
     */
    public function decrypt($data, $sector = null)
    {
        if ($sector == null) {
            $sector = $this->sector;
        }
        if (strlen($data) % $this->blocksize) {
            return false;
        }
        $out = '';
        while ($data) {
            $tweak = $this->getTweak($sector);
            $out .= $this->decryptSector(substr($data, 0, $this->sectorSize), $tweak);
            $data = substr($data, $this->sectorSize, strlen($data) - $this->sectorSize);
            $sector += 1;
        }
        return $out;
    }

    /**
     * Function that decrypts a sector
     *
     * @param string $data  data to decrypt
     * @param string $tweak tweaked sector address space
     *
     * @return string decrypted sector
     */
    private function decryptSector($data, $tweak)
    {
        if (strlen($data) % $this->blocksize) {
            return false;
        }
        $out = '';
        $tweak = $this->K2->encrypt(hex2bin(sprintf('%032x', $tweak)));
        while ($data) {
            $out .= sxor($tweak, $this->K1->decryptBlockEcb(sxor(substr($data, 0, 0x10), $tweak)));
            $t = new BINSTRNUM(strrev($tweak));
            $t->mult(2);
            if (strlen($t->binstr) > 16) {
                $t->binstr = sxor($t->binstr, hex2bin("0100000000000000000000000000000087"));
                $t->binstr = substr($t->binstr, 1, strlen($t->binstr) - 1);
            }
            $tweak = strrev($t->binstr);
            $data = substr($data, 0x10, strlen($data) - 0x10);
        }
        return $out;
    }

    /**
     * Function that adds the sector paramater to the object
     *
     * @param string $sector the sector number
     *
     * @return string the sector number
     */
    private function setSector($sector)
    {
        $this->sector = $sector;
    }

    /**
     * Function that adds the sector size paramater to the object
     *
     * @param string $sectorSize the sector size
     *
     * @return string the sector size
     */
    private function setSectorSize($sectorSize)
    {
        $this->sectorSize = $sectorSize;
    }
}
