<?php

/**
 * NSZDecompress class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to decompress a NSZ file
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

class NSZDecompress
{
    /**
     * Creates properties and values for object
     *
     * @param string $path path of the NSZ file
     * @param string $keys RSA keys to used for decryption
     *
     * @return mixed properties and values of rsa keys
     */
    public function __construct($path, $keys = null)
    {
        $this->nszfile = new NSP($path, $keys);
        $this->nszfile->getHeaderInfo();
        $this->ncadecompressedsize = $this->nszfile->nczfile->getOriginalSize();
    }

    /**
     * Sets the Headers for the individual files in the NSZ
     *
     * @return string the verification status
     */
    public function headerChange()
    {
        $sizediff = $this->ncadecompressedsize - $this->nszfile->filesList[$this->nszfile->nczfileidx]->filesize;
        fseek($this->nszfile->fh, 0);
        $srcHeader = fread($this->nszfile->fh, $this->nszfile->fileBodyOffset/*+$this->nszfile->filesList[0]->fileoffset*/);
        $dstHeader = $srcHeader;
        $dstOffset = 0;
        for ($i = 0; $i < count($this->nszfile->filesList); $i++) {
            if ($i >= $this->nszfile->nczfileidx) {
                $dataOffset = $dstOffset;/*$this->nszfile->filesList[$i]->fileoffset+$sizediff;*/
                $dataSize = $this->nszfile->filesList[$i]->filesize;
                if ($i == $this->nszfile->nczfileidx) {
                    $dataOffset = $dstOffset;
                    $dataSize = $this->ncadecompressedsize;
                }
                $dstOffset = $dataSize + $dataOffset;
                $dstHeader = substr($dstHeader, 0, $this->nszfile->filesList[$i]->dataFieldoffset) . pack("P", $dataOffset) . pack("P", $dataSize) . substr($dstHeader, $this->nszfile->filesList[$i]->dataFieldoffset + 16);
            } else {
                $dataOffset = $dstOffset;/*$this->nszfile->filesList[$i]->fileoffset+$sizediff;*/
                $dataSize = $this->nszfile->filesList[$i]->filesize;
                $dstOffset = $dataSize + $dataOffset;
                $dstHeader = substr($dstHeader, 0, $this->nszfile->filesList[$i]->dataFieldoffset) . pack("P", $dataOffset) . pack("P", $dataSize) . substr($dstHeader, $this->nszfile->filesList[$i]->dataFieldoffset + 16);
            }
        }


        $dstHeader = substr_replace($dstHeader, "a", $this->nszfile->nczfilestringoffset + strlen($this->nszfile->filesList[$this->nszfile->nczfileidx]->name) - 1) . substr($dstHeader, $this->nszfile->nczfilestringoffset + strlen($this->nszfile->filesList[$this->nszfile->nczfileidx]->name));
        print($dstHeader);
        for ($i = 0; $i < count($this->nszfile->filesList); $i++) {
            fseek($this->nszfile->fh, $this->nszfile->fileBodyOffset + $this->nszfile->filesList[$i]->fileoffset);
            if ($i == $this->nszfile->nczfileidx) {
                $this->nszfile->nczfile->decompress();
            } else {
                $n = 0;
                while ($n < $this->nszfile->filesList[$i]->filesize) {
                    $chunkSz = 0x10000;
                    if ($this->nszfile->filesList[$i]->filesize - $n > 0x10000) {
                        $chunkSz = 0x10000;
                    } else {
                        $chunkSz = $this->nszfile->filesList[$i]->filesize - $n;
                    }
                    print(fread($this->nszfile->fh, $chunkSz));
                    $n += $chunkSz;
                }
            }
        }
    }
}
