<?php

/**
 * NCZ class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to analyze nca file
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class NCZ
{
    /**
     * Creates properties and values for object
     *
     * @param string $fh         resource to read from
     * @param string $fileOffset where in the file to start reading from
     * @param string $fileSize   size of the file
     * @param array  $keys       rsa keys to use
     *
     * @return mixed properties and values of the ncz file
     */
    public function __construct($fh, $fileOffset, $fileSize, $keys = null)
    {
        $this->nczfile = new NCA($fh, $fileOffset, $fileSize, $keys);
        $this->fileOffset = $fileOffset;
        $this->keys = $keys;
        $this->fh = $fh;
        $this->useBlockCompression = false;
        $this->compressedSize = $fileSize;
    }

    /**
     * Reads and returns header info
     *
     * @return mixed properties and values of the ncz header
     */
    public function readHeader()
    {
        $this->nczfile->readHeader();
    }

    /**
     * Gets uncompressed filesize
     *
     * @return string original file size of the section
     */
    public function getOriginalSize()
    {
        $tmpsize = 0x4000;
        for ($i = 0; $i < count($this->sections); $i++) {
            $tmpsize += $this->sections[$i]->size;
        }
        return $tmpsize;
    }

    /**
     * Gets information on the NCZ sectors
     *
     * @return mixed Sector information of the NCZ file
     */
    public function readNczSect()
    {
        fseek($this->fh, $this->fileOffset + 0x4000);
        $magic = fread($this->fh, 8);
        if ($magic != "NczSectN") {
            return false;
        }
        $sectionCount = unpack("P", fread($this->fh, 8))[1];
        $this->sections = array();
        for ($i = 0; $i < $sectionCount; $i++) {
            $tmpsection = new \stdClass();
            $tmpsection->fileOffset = unpack("P", fread($this->fh, 8))[1];
            $tmpsection->size = unpack("P", fread($this->fh, 8))[1];
            $tmpsection->cryptoType = unpack("P", fread($this->fh, 8))[1];
            fread($this->fh, 8);
            $tmpsection->cryptoKey =  fread($this->fh, 16);
            $tmpsection->cryptoCounter =  fread($this->fh, 16);
            $this->sections[] = $tmpsection;
        }
        $secpos = ftell($this->fh);
        $blockMagic =  fread($this->fh, 8);
        if ($blockMagic == "NczBlock") {
            $this->useBlockCompression = true;
        }
        fseek($this->fh, $secpos);
    }

    /**
     * Gets information on the NCZ blocks
     *
     * @return mixed Sector information of the NCZ blocks
     */
    private function readNczBlock()
    {
        $tmpblk = new StdClass();
        $tmpblk->magic = fread($this->fh, 8);
        $tmpblk->version = fread($this->fh, 1);
        $tmpblk->type = fread($this->fh, 1);
        fread($this->fh, 1);
        $tmpblk->blockSizeExponent = fread($this->fh, 1);
        $tmpblk->numberOfBlocks  = unpack("V", fread($this->fh, 4))[1];
        $tmpblk->decompressedSize = unpack("P", fread($this->fh, 8))[1];
        $tmpblk->compressedBlockSizeList = array();
        for ($i = 0; $i < $tmpblk->numberOfBlocks; $i++) {
            $tmpblk->compressedBlockSizeList[]  = unpack("V", fread($this->fh, 4))[1];
        }
    }

    /**
     * Gets offset of the CTR
     *
     * @param $startCtr   beginning of the CTR
     * @param $fileOffset used to calculate the CTR offset address
     *
     * @return mixed string CTR offset
     */
    public function getCtrOffset($startCtr, $fileOffset)
    {
        $ctr = new CTRCOUNTERGMP($startCtr);
        $adder = $fileOffset / 16;
        $ctr->add($adder);
        return $ctr->getCtr();
    }

    /**
     * Decompresses NCZ
     *
     * @return mixed decompressed data
     */
    public function decompress()
    {
        fseek($this->fh, $this->fileOffset);
        print(fread($this->fh, 0x4000));
        $this->readNczSect();
        if ($this->useBlockCompression) {
            return false;
        }
        $start = ftell($this->fh);
        $yazstd = new yazstd_decompress();
        $sectionbuffer = "";

        foreach ($this->sections as $section) {
            $crypto = new AESCTR($section->cryptoKey, $section->cryptoCounter, true);
            $end = $section->fileOffset + $section->size;
            $i = $section->fileOffset;

            $startCtr = $section->cryptoCounter;
            while ($i < $end) {
                $chunkSz = 0x10000;
                if ($end - $i > 0x10000) {
                    $chunkSz = 0x10000;
                } else {
                    $chunkSz = $end - $i;
                }

                $compressedChunk = fread($this->fh, $chunkSz);

                $inputChunk  = $yazstd->decompress($compressedChunk);

                if (strlen($sectionbuffer) > 0) {
                    $inputChunk = $sectionbuffer . $inputChunk;
                    $sectionbuffer = "";
                }

                if ($i + strlen($inputChunk) > $section->size) {
                    $outsectionlen = $i + strlen($inputChunk) - $end;
                    $sectionbuffer = substr($inputChunk, strlen($inputChunk) - $outsectionlen);
                    $inputChunk = substr($inputChunk, 0, strlen($inputChunk) - $outsectionlen);
                }

                if ($section->cryptoType == 3 || $section->cryptoType == 4) {
                    $ctr = $this->getCtrOffset($startCtr, $i);

                    $inputChunk = $crypto->encrypt($inputChunk, $ctr);
                }

                print($inputChunk);
                $i += strlen($inputChunk);
                flush();
            }
        }
    }

    /**
     * Returns information about the NCZ file
     *
     * @return mixed NCZ file information
     */
    public function analyze()
    {
        $this->readHeader();
        $this->readNczSect();
        $this->nczfile->getFs();
        $ncafilesList = array();
        $retinfo = new \stdClass();
        $retinfo->rsa1 = strtoupper($this->nczfile->rsa1);
        $retinfo->rsa2 = strtoupper($this->nczfile->rsa2);
        $retinfo->magic = $this->nczfile->magic;
        $retinfo->useBlockCompression = $this->useBlockCompression;
        $retinfo->compressedSize = $this->compressedSize;
        $retinfo->sigcheckrsa1 = $this->nczfile->sigcheck;
        $retinfo->sigcheckrsa2 = null;
        $retinfo->distributionType =  $this->nczfile->distributionType;
        $retinfo->contentType = $this->nczfile->contentType;
        $retinfo->contentSize =  $this->nczfile->contentSize;
        $retinfo->programId =  strtoupper($this->nczfile->programId);
        $retinfo->rightsId = strtoupper(strrev($this->nczfile->rightsId));
        $retinfo->contentIndex =  $this->nczfile->contentIndex;
        $retinfo->sdkArray = $this->nczfile->sdkArray;
        $retinfo->crypto_type = $this->nczfile->crypto_type;

        $retinfo->enckeyArea = $this->nczfile->enckeyArea;
        $retinfo->deckeyArea = $this->nczfile->deckeyArea;
        $retinfo->enctitlekey = $this->nczfile->enctitlekey;
        $retinfo->dectitlekey = $this->nczfile->dectitlekey;
        $retinfo->ncafilesList = $ncafilesList;
        $retinfo->pfs0idx =  $this->nczfile->pfs0idx;
        $retinfo->romfsidx =  $this->nczfile->romfsidx;
        $retinfo->pfs0Logoidx =  $this->nczfile->pfs0Logoidx;
        $retinfo->romfspatchidx =  $this->nczfile->romfspatchidx;
        $retinfo->sections = array(false,false,false,false);

        return $retinfo;
    }
}
