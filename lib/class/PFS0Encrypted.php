<?php

/**
 * PFS0Encrypted class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Test Class for decrypt on the fly contents and not store in memeory (make possibile to extract big files if needed)
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class PFS0Encrypted
{
    public $cnmt;
    public $filesList;
    public $isexefs;
    public $npdm;
    public $aesctr;
    public $data;
    public $dataSize;
    public $fh;
    public $fileBodyOffset;
    public $numFiles;
    public $pfs0header;
    public $pfs0Offset;
    public $startctr;
    public $startOffset;
    public $stringTableOffset;
    public $stringTableSize;

    /**
     * Creates properties and values for object
     *
     * @param string $fh            file handler
     * @param string $encDataOffset file offset for encrypted data
     * @param string $encSize       size of encrypted data
     * @param string $pfs0Offset    file offset for the pfs0 partition
     * @param string $pfs0Size      size of the pfs0 partition
     * @param string $key           key to use for decryption
     * @param string $ctr           CTR for file
     *
     * @return mixed properties and values of rsa keys
     */
    public function __construct($fh, $encDataOffset, $encSize, $pfs0Offset, $pfs0Size, $key, $ctr)
    {
        $this->fh = $fh;
        $this->startctr = hex2bin($ctr);
        fseek($this->fh, $encDataOffset + $pfs0Offset);
        $this->aesctr = new AESCTR(hex2bin(strtoupper($key)), hex2bin(strtoupper($ctr)), true);
        $this->data = $this->aesctr->decrypt(fread($this->fh, 0x10), $this->getCtrOffset($pfs0Offset));
        $this->startOffset = $encDataOffset + $pfs0Offset;
        $this->pfs0Offset = $pfs0Offset;
        $this->dataSize = $pfs0Size;
        $this->isexefs = false;
    }

    /**
     * Get CTR file address
     * offset must be a multiple of 0x10
     *
     * @param string $offset offset of the CTR
     *
     * @return mixed properties and values of rsa keys
     */
    public function getCtrOffset($offset)
    {
        $ctr = new CTRCOUNTERGMP($this->startctr);
        $adder = $offset / 16;
        $ctr->add($adder);
        return $ctr->getCtr();
    }

    /**
     * Gets header info about the PFS0 partition
     *
     * @return mixed header info
     */
    public function getHeader()
    {
        $this->pfs0header = substr($this->data, 0, 0x04);
        if ($this->pfs0header != "PFS0") {
            return false;
        }
        $this->numFiles = unpack("V", substr($this->data, 4, 0x04))[1];
        $this->stringTableSize = unpack("V", substr($this->data, 8, 0x04))[1];
        fseek($this->fh, $this->startOffset);
        $this->stringTableOffset = 0x10 + 0x18 * $this->numFiles;
        $this->fileBodyOffset = $this->stringTableOffset + $this->stringTableSize;
        $this->data = $this->aesctr->decrypt(fread($this->fh, $this->fileBodyOffset), $this->getCtrOffset($this->pfs0Offset));
        $this->filesList = [];
        for ($i = 0; $i < $this->numFiles; $i++) {
            $dataOffset = unpack("P", substr($this->data, 0x10 + (0x18 * $i), 0x08))[1];
            $dataSize = unpack("P", substr($this->data, 0x10 + 0x08 + (0x18 * $i), 0x08))[1];
            $stringOffset = unpack("V", substr($this->data, 0x10 + 0x08 + 0x08 + (0x18 * $i), 0x04))[1];
            $filename = "";
            $n = 0;
            while (true && $this->stringTableOffset + $stringOffset + $n < $this->dataSize - 1) {
                $byte = unpack("C", substr($this->data, $this->stringTableOffset + $stringOffset + $n, 1))[1];
                if ($byte == 0x00) {
                    break;
                }
                $filename = $filename . chr($byte);
                $n++;
            }
            $parts = explode('.', strtolower($filename));
            $file = new \stdClass();
            $file->name = $filename;
            $file->size = $dataSize;
            $file->offset = $dataOffset;
            $this->filesList[] = $file;

            if ($parts[count($parts) - 1] == "cnmt") {
                $this->cnmt = new CNMT($this->getFile($i), $dataSize);
            }
            if ($parts[count($parts) - 1] == "npdm") {
                $this->isexefs = true;
                $this->npdm = new NPDM($this->getFile($i), $dataSize);
            }
        }
    }

    /**
     * In memory extraction use on small file only
     *
     * @param $idx index of the file to download
     *
     * @return mixed data file
     */
    public function getFile($idx)
    {
        $subber = ($this->fileBodyOffset + $this->filesList[$idx]->offset) % 16;
        fseek($this->fh, $this->startOffset + $this->fileBodyOffset + $this->filesList[$idx]->offset - $subber);

        $decfile = $this->aesctr->decrypt(fread($this->fh, $this->filesList[$idx]->size + $subber), $this->getCtrOffset($this->pfs0Offset + $this->fileBodyOffset + $this->filesList[$idx]->offset - $subber));
        $decfile = substr($decfile, $subber, $this->filesList[$idx]->size + $subber);
        return $decfile;
    }

    /**
     * File extraction for larger files
     *
     * @param $idx index of the file to download
     *
     * @return mixed data file
     */
    public function extractFile($idx)
    {
        $subber = ($this->fileBodyOffset + $this->filesList[$idx]->offset + $this->pfs0Offset) % 16;
        $size = $this->filesList[$idx]->size;
        $tmpchunksize = $size + $subber;
        fseek($this->fh, $this->startOffset + $this->fileBodyOffset + $this->filesList[$idx]->offset - $subber);

        $chunksize = 5 * (1024 * 1024);
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $size);
        header('Content-Disposition: attachment;filename="' . $this->filesList[$idx]->name . '"');
        $tmpchunksize = $size + $subber;
        $tmpchunkdone = 0;
        while ($tmpchunksize > $chunksize) {
            $ctr = $this->getCtrOffset($this->pfs0Offset + $this->fileBodyOffset + $this->filesList[$idx]->offset - $subber + ($chunksize * $tmpchunkdone));
            $outdata =  $this->aesctr->decrypt(fread($this->fh, $chunksize), $ctr);
            if ($tmpchunkdone == 0) {
                print(substr($outdata, $subber));
            } else {
                print($outdata);
            }
            $tmpchunksize -= $chunksize;
            $tmpchunkdone += 1;

            flush();
        }

        if ($tmpchunksize <= $chunksize) {
            $ctr = $this->getCtrOffset($this->pfs0Offset + $this->fileBodyOffset + $this->filesList[$idx]->offset - $subber + ($chunksize * $tmpchunkdone));
            $outdata = $this->aesctr->decrypt(fread($this->fh, $tmpchunksize), $ctr);
            if ($tmpchunkdone == 0) {
                print(substr($outdata, $subber));
            } else {
                print($outdata);
            }
            flush();
        }
    }
}
