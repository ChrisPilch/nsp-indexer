<?php

/**
 * AESCTR class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to add key ctr and ssl information to an object
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class AESCTR
{
    public $ctr;
    public $key;
    public $ssl;

    /**
     * Adds properties and values to the object
     *
     * @param string $key key to add
     * @param string $ctr initialization vector to add if provided
     * @param string $ssl used to check if data is encrypted
     *
     * @return mixed properties and values of ssl
     */
    public function __construct($key, $ctr, $ssl = true)
    {
        $this->ssl = $ssl;
        if ($ssl == true) {
            $this->ctr = $ctr;
            $this->key = $key;
        } else {
            $this->aes = new AESECB($key);
            if (strlen($ctr) != $this->aes->blocksize) {
                return false;
            }
            $this->ctr = $ctr;
        }
    }

    /**
     * Function to decrypt data
     *
     * @param string $data data to decrypt
     * @param string $ctr  initialization vector
     *
     * @return mixed decrypted data
     */
    public function decrypt($data, $ctr = null)
    {
        if ($this->ssl) {
            if ($ctr == null) {
                $ctr = $this->ctr;
            }
            $out = openssl_decrypt($data, 'AES-128-CTR', $this->key, OPENSSL_RAW_DATA, $ctr);
            return $out;
        } else {
            // same CTR is symmetric
            return $this->encrypt($data, $ctr);
        }
    }

    /**
     * Function used to encrypt data
     *
     * @param string $data data to encrypt
     * @param string $ctr  initialization vector
     *
     * @return mixed encrypted data
     */
    public function encrypt($data, $ctr = null)
    {
        if ($this->ssl) {
            if ($ctr == null) {
                $ctr = $this->ctr;
            }
            $out = openssl_encrypt($data, 'AES-128-CTR', $this->key, OPENSSL_RAW_DATA, $ctr);
        } else {
            if ($ctr == null) {
                $ctr = $this->ctr;
            }
            $out = '';
            $ln = strlen($data);
            while ($ln) {
                $xorpad = $this->aes->encryptBlockEcb($ctr->getCtr());
                $l = min(0x10, $ln);
                $out .= sxor(substr($data, 0, $l), substr($xorpad, 0, $l));
                $data = substr($data, $l, strlen($data) - $l);
                $ln -= $l;
                $ctr->add(1);
            }
        }
        return $out;
    }
}
