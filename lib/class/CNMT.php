<?php

/**
 * CNMT class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to add the media type to an object
 * mediaType 0x80 Application (Base Game), 0x81 Patch Update, 0x82 AddOnContent (DLC)
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class CNMT
{
    public $id;
    public $mediaType;
    public $otherId;
    public $reqsysversion;
    public $version;

    /**
     * Creates properties and values for object
     *
     * @param string $data     string to parse
     * @param string $dataSize size of the data
     *
     * @return mixed properties and values of the cnmt info
     */
    public function __construct($data, $dataSize)
    {
        $data;
        $this->id = bin2hex(strrev(substr($data, 0, 0x8)));
        $this->version = unpack("V", (substr($data, 0x08, 0x4)))[1];
        $this->mediaType = substr($data, 0x0c, 0x1);
        $this->otherId = bin2hex(strrev(substr($data, 0x20, 0x08)));
        $this->reqsysversion = unpack("V", (substr($data, 0x28, 0x4)))[1];
    }
}
