<?php

/**
 * IVFC class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to determine filesystem metadata from the superblock
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class IVFC
{
    public $sboffset;
    public $shahash;
    public $magic;
    public $magincnum;
    public $levels;

    /**
     * Calculates filesystem metadata from the superblock
     *
     * @param string $superBlock data to use to calculate partition info
     *
     * @return mixed properties and values of rsa keys
     */
    public function __construct($superBlock)
    {
        $this->magic = substr($superBlock, 0, 4);
        $this->magincnum = unpack("V", substr($superBlock, 0x4, 4))[1];
        $this->levels = array();
        $this->sboffset = unpack("P", substr($superBlock, 0x88, 8))[1];
        for ($i = 0; $i < 6; $i++) {
            $sofs = 0x10 + ($i * 0x18);
            $tmplevel = new \stdClass();
            $tmplevel->offset = unpack("P", substr($superBlock, $sofs, 0x08))[1];
            $tmplevel->size = unpack("P", substr($superBlock, $sofs + 0x08, 0x08))[1];
            $tmplevel->blockSize = 1 << unpack("V", substr($superBlock, $sofs + 0x08 + 0x08, 4))[1];
            $this->levels[] = $tmplevel;
        }
        $this->shahash = substr($superBlock, 0xc0, 0x20);
    }
}
