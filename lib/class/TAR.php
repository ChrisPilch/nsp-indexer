<?php

/**
 * TAR class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * PHP implementation of tar
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class TAR
{
        /**
         * Creates properties and values for object
         *
         * @return NULL
         */
    public function __construct()
    {
    }

    /**
     * Adds file to tar archive
     *
     * @param string $filename name of the file
     * @param string $fh       file handler for the file being written to
     * @param string $offset   where to start writing data
     * @param string $size     size of the file being added
     *
     * @return string function completion message
     */
    public function addFile($filename, $fh, $offset, $size)
    {
        $chunksize = 5 * (1024 * 1024);
        $entry = $this->getTarHeaderFooter($filename, $size, 0);
        print($entry[0]);
        $tmpsize = $size;

        fseek($fh, $offset);

        while ($tmpsize > 0) {
            if ($tmpsize > $chunksize) {
                print(fread($fh, $chunksize));
                $tmpsize -= $chunksize;
                flush();
            } else {
                print(fread($fh, $tmpsize));
                $tmpsize = 0;
                flush();
            }
        }
        print($entry[1]);
        flush();
    }

    /**
     * Get the tar header and footer
     *
     * @param string $filename  file to read header and footer from
     * @param string $filesize  filesize of the tar archive
     * @param string $filemtime mtime for the archive
     *
     * @return array tar file information
     */
    public function getTarHeaderFooter($filename, $filesize, $filemtime)
    {
        $return = pack("a100a8a8a8a12a12", $filename, 644, 0, 0, decoct($filesize), decoct($filemtime));
        $checksum = 8 * 32; // space for checksum itself
        for ($i = 0; $i < strlen($return); $i++) {
            $checksum += ord($return[$i]);
        }
        $return .= sprintf("%06o", $checksum) . "\0 ";
        return array(
        $return . str_repeat("\0", 512 - strlen($return)),
        str_repeat("\0", 511 - ($filesize + 511) % 512)
        );
    }
}
