<?php

/**
 * PFS0 class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to get header info and download files from the PFS0 partition
 * PFS0 support is just a PARTIAL implementation just to match our needs
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class PFS0
{
    public $filesList;
    public $fh;
    public $dataOffset;
    public $dataSize;
    public $pfs0Offset;
    public $pfs0Size;
    public $startOffset;
    public $data;
    public $pfs0header;
    public $numFiles;
    public $stringTableSize;
    public $stringTableOffset;
    public $fileBodyOffset;

    /**
     * Creates properties and values for object
     *
     * @param string $fh         file handler
     * @param string $dataOffset offset of the file
     * @param string $dataSize   size of the data
     * @param string $pfs0Offset offset for the pfs0 partition
     * @param string $pfs0Size   size of the pfs0 partition
     *
     * @return mixed data file from the pfs0 partition
     */
    public function __construct($fh, $dataOffset, $dataSize, $pfs0Offset, $pfs0Size)
    {
        $this->fh  = $fh;
        $this->dataOffset = $dataOffset;
        $this->dataSize = $dataSize;
        $this->pfs0Offset = $pfs0Offset;
        $this->pfs0Size = $pfs0Size;
        $this->startOffset = $dataOffset + $pfs0Offset;
        fseek($this->fh, $dataOffset + $pfs0Offset);
        $this->data = fread($this->fh, 0x10);
    }

    /**
     * Reads and returns header info
     *
     * @return mixed properties and values from the pfs0 header
     */
    public function getHeader()
    {
        $this->pfs0header = substr($this->data, 0, 0x04);
        if ($this->pfs0header != "PFS0") {
            return false;
        }
        $this->numFiles = unpack("V", substr($this->data, 4, 0x04))[1];
        $this->stringTableSize = unpack("V", substr($this->data, 8, 0x04))[1];
        fseek($this->fh, $this->startOffset);

        $this->stringTableOffset = 0x10 + 0x18 * $this->numFiles;
        $this->fileBodyOffset = $this->stringTableOffset + $this->stringTableSize;
        $this->data = fread($this->fh, $this->fileBodyOffset);
        $this->stringTableOffset = 0x10 + 0x18 * $this->numFiles;
        $this->fileBodyOffset = $this->stringTableOffset + $this->stringTableSize;
        $this->filesList = [];
        for ($i = 0; $i < $this->numFiles; $i++) {
            $dataOffset = unpack("P", substr($this->data, 0x10 + (0x18 * $i), 0x08))[1];
            $dataSize = unpack("P", substr($this->data, 0x10 + 0x08 + (0x18 * $i), 0x08))[1];
            $stringOffset = unpack("V", substr($this->data, 0x10 + 0x08 + 0x08 + (0x18 * $i), 0x04))[1];
            $filename = "";
            $n = 0;
            while (true && $this->stringTableOffset + $stringOffset + $n < $this->dataSize - 1) {
                $byte = unpack("C", substr($this->data, $this->stringTableOffset + $stringOffset + $n, 1))[1];
                if ($byte == 0x00) {
                    break;
                }
                $filename = $filename . chr($byte);
                $n++;
            }
            $parts = explode('.', strtolower($filename));
            $file = new \stdClass();
            $file->name = $filename;
            $file->size = $dataSize;
            $file->offset = $dataOffset;
            $this->filesList[] = $file;
        }
    }

    /**
     * Extracts file for download
     *
     * @param $idx index of the file to download
     *
     * @return mixed data file
     */
    public function extractFile($idx)
    {
        fseek($this->fh, $this->startOffset + $this->fileBodyOffset + $this->filesList[$idx]->offset);

        $size = $this->filesList[$idx]->size;
        $chunksize = 5 * (1024 * 1024);
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $size);
        header('Content-Disposition: attachment;filename="' . $this->filesList[$idx]->name . '"');
        $tmpchunksize = $size;
        $tmpchunkdone = 0;
        while ($tmpchunksize > $chunksize) {
            $outdata =  fread($this->fh, $chunksize);
            print($outdata);
            $tmpchunksize -= $chunksize;
            $tmpchunkdone += 1;

            flush();
        }
        if ($tmpchunksize <= $chunksize) {
            $outdata = fread($this->fh, $tmpchunksize);
            print($outdata);
            flush();
        }
    }
}
