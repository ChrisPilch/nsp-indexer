<?php

/**
 * ACID class file
 *
 * PHP version 8.2
 *
 * @category NSP-Indexer
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/

namespace NSPIndexer;

/**
 * Class to set rsa keys, magic and size for the ACID header
 *
 * @category Class
 * @package  NSP-Indexer
 * @author   proconsule <proconsule@github.com>
 * @author   jangrewe <jangrewe@github.com>
 * @author   izenn <izenn@gitlab.com>
 * @license  BY-NC-SA https://creativecommons.org/licenses/by-nc-sa/4.0/
 * @link     https://gitlab.com/izenn/nsp-indexer
 **/
class ACID
{
    public $rsa1;
    public $rsa2;
    public $magic;
    public $size;

    /**
     * Creates properties and values for object
     *
     * @param string $data data to parse
     * @param string $size size of the ACID header
     *
     * @return mixed properties and values of rsa keys
     */
    public function __construct($data, $size)
    {
        $this->rsa1 = bin2hex(substr($data, 0x00, 0x100));
        $this->rsa2 = bin2hex(substr($data, 0x100, 0x100));
        $this->magic = substr($data, 0x200, 0x04);
        $this->size = unpack("V", substr($data, 0x204, 0x04))[1];
    }
}
