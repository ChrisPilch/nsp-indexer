# NSP Indexer
> PHP Indexer for Switch NSP (XCI NSZ XCZ) by proconsule and jangrewe

![Preview](docs/preview.jpg)

# How To
Place all files into a directory on your webserver, then copy `config.defaults.php` to `config.php` and adjust it to your needs.

To get the titledb.json and versions.json files: git clone https://github.com/izenn/nut.git, run nut.py, and update the titledb path in config.php. Add nut to cron so that the json files get updated on a regular basis.

Your filenames need to contain at least a Title ID in the format `[0100XXXXXXXXYYYY]`, and Updates also need a version tag like `[v1441792]`.

For advanced features a 64 bit OS is needed!

For NSZ decompression you need this PHP Extension https://github.com/proconsule/php-ext-yazstd

If you want to do a quick test you can use docker, enter in docker directory and use docker-compose up

Hope you enjoy it!

# Features
- List NSP, XCI, NSZ and XCZ titles in a fancy way (Base Games, DLCs and Updates)
- Icon,Banner and Screenshot of titles
- Check For latest Update version of game file (if any)
- Compatible with tinfoil Custom Index JSON (if called with `index.php/?tinfoil`)
- Compatible with DBI plaintext list (if called with `index.php/?DBI`)
- Net Install (if TCP port 2000 of Switch is reachable by webserver)
- NSP Internal TitleID Check
- XCI Internal TitleID Check (if keys supplied)
- NSP & XCI File Decryption
- NCA Header Signature Check
- XCI Header Signature Check
- NCA Analyzer (Show relevant info of NCA)
- Download of individual internal file (NCA TIK XML CERT)
- Extraction of files inside NCA (PFS0 an RomFS support)
- Download of Switch FW Update from XCI file (as a single tar file)
- NSZ on the fly Decompression (see FAQ)
- File Rename Based on TitleID & Version
- File Upload

# Known Issue
- 32Bit System suffer for >2GB limit in many way (fseek and so on) so some features are not working like Rom Info. for Windows users use php > 7.0 as also on 64bit machines lower versions have 32bit integers.

# FAQ
**Question:** I am a 32bit system and rom info button doesn't show, why?

**Answer** Rom info button is disabled on 32bit system. sorry but with php on a 32bit system is impossible to do decryption

**Q:** What is the differnces between master and dev branch?

**A:** Master branch is stable and updated only when all features are tested and stable. Dev branch often have more features but may (mostly with proconsule commits) have bugs.

**Q:** I found a bug, where i can report that?

**A:** Here on github as usual, or on GBAtemp forum here https://gbatemp.net/threads/nsp-indexer.591541/

**Q:** How can i do NSZ Decompression?

**A:** NSZ is a smart format created by nicoboss with the help of some other developers you can find it here https://github.com/nicoboss/nsz it uses zStandard compression. Doing decompression all in PHP is really hard so i created a PHP extension to help in zStandard decompression you can find it here https://github.com/proconsule/php-ext-yazstd . Compile and install into your system and you can decompress NSZ on the fly with NSP Indexer (or you can use docker that will prepare all for you including NSZ decompression)

# third-party libraries used
- Bootstrap https://getbootstrap.com/
- jQuery https://jquery.com/
- jQuery Lazy http://jquery.eisbehr.de/lazy/
- jQuery Confirm https://craftpip.github.io/jquery-confirm/
- LightBox2 https://lokeshdhakar.com/projects/lightbox2/
- Flow.js https://github.com/flowjs/flow.js/
- Flow.js php server https://github.com/flowjs/flow-php-server
- pspseclib https://github.com/phpseclib/phpseclib (only a small part used)
- some small parts of code was taken from forums/articles around the web, references are inside the code

# Thanks to
- SciresM for aes128.py we ported to PHP for NCA decryption
- duckbill007 for support on DBI Installer
- blawar for nsp update version look suggestion and all tinfoil cool stuff
- nicoboss for help on supporting NSZ XCZ (your file type is so smart!)
- Ejec at GBAtemp forum (for his bugs reports)
